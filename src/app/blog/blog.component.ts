import { Component, OnInit } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {

  myCarouselImages = [61,62,63,64,65,66,67,68].map((i) => `https://mdbootstrap.com/img/Photos/Slides/img%20(${i}).jpg`);
  mySlideOptions = { items: 3, dots: true, nav: false, responsiveClass: true, responsive: {
    0: {
      items: 1,
      nav: true
    },
    600: {
      items: 3,
      nav: false
    },
    1000: {
      items: 5,
      nav: true,
      loop: false
    }
  }};
  myCarouselOptions = { items: 3, dots: true, nav: true };
  mySlideImages = [61,62,63,64,65,66,67,68].map((i) => `https://mdbootstrap.com/img/Photos/Slides/img%20(${i}).jpg`);

  images = [1, 2, 3, 4].map(() => `https://picsum.photos/900/500?random&t=${Math.random()}`);

  constructor(config: NgbCarouselConfig) {
    // customize default values of carousels used by this component tree
    config.interval = 10000;
    config.wrap = false;
    config.keyboard = false;
    config.pauseOnHover = false;
  }

  ngOnInit() {
  }

}
